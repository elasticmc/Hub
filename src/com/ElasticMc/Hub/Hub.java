package com.ElasticMc.Hub;

import com.ElasticMc.Hub.Player.PlayerJoin;
import com.ElasticMc.Hub.Player.PlayerDamage;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Hub extends JavaPlugin {

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(new PlayerJoin(), this);
        getServer().getPluginManager().registerEvents(new PlayerDamage(), this);
    }

    @Override
    public void onDisable()
    {

    }
}